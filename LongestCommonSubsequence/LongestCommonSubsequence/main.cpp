#include<iostream>
#include<vector>

using namespace std;

class Engine
{
private:
	   string str1;
	   string str2;
	   string lcsString;
	   vector< vector<int> > matrixVector;
    
	   void initializeVector()
	   {
           int s1Len = (int)str1.length();
           int s2Len = (int)str2.length();
           for(int i = 0 ; i < s1Len ; i++)
           {
               vector<int> innerVector;
               for(int j = 0 ; j < s2Len ; j++)
               {
                   innerVector.push_back(0);
               }
               matrixVector.push_back(innerVector);
           }
       }
    
public:
	   Engine(string s1 , string s2)
	   {
           str1 = s1;
           str2 = s2;
           initializeVector();
       }
	   string findLCS()
	   {
           int s1Len = (int)str1.length();
           int s2Len = (int)str2.length();
           for(int i = 0 ; i < s1Len ; i++)
           {
               for(int j = 0 ; j < s2Len ;j++)
               {
                   if(str1[i] == str2[j])
                   {
                       if(!i || !j)
                       {
                           matrixVector[i][j] = 1;
                       }
                       else
                       {
                           matrixVector[i][j] = matrixVector[i-1][j-1] + 1;
                       }
                       if((int)lcsString.length() < matrixVector[i][j])
                       {
                           lcsString = "";
                           int tempI = i;
                           int tempJ = j;
                           while (tempI >= 0 && tempJ >= 0)
                           {
                               lcsString = str1[tempI] + lcsString;
                               tempI--;
                               tempJ--;
                           }
                       }
                   }
               } 
           }
           return lcsString;
       }
};

int main()
{
    string str1 = "abcdabc";
    string str2 = "cdab";
    Engine e    = Engine(str1 , str2);
    cout<<e.findLCS()<<endl;
    return 0;
}
